import { AppLocale } from '../types/locales'
import * as de from './de/translations.json'
import * as en from './en/translations.json'
import * as cs from './cs/translations.json'
import * as es from './es/translations.json'

export type Translation = {
    [key: string]: string
}

export type Translations = Record<AppLocale, Translation>

export const t: Translations = {
    de,
    en,
    cs,
    es,
}
