export type AppLocale = 'en' | 'de' | 'cs' | 'es'

export type AppLocaleProviderProps = {
    children: React.ReactNode
    locale: AppLocale
}
