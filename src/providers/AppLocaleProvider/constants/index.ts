import * as React from 'react'
import { AppLocale } from '../types'

export const defaultLocale = 'en'

export const LocaleContext = React.createContext<AppLocale>(defaultLocale)
