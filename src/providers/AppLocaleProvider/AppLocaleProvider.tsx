import React from 'react'
import { wrap } from '../../utils/globalUtils'
import { useAppLocaleProviderProps, useAppLocaleProvider } from './hooks/useAppLocaleProvider'
import AppLocaleProviderView from './views/AppLocaleProviderView'

const AppLocaleProvider = wrap(AppLocaleProviderView, useAppLocaleProvider) as React.FC<useAppLocaleProviderProps>
export default AppLocaleProvider
