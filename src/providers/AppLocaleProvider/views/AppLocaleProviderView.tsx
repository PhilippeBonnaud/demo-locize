import { LocaleContext } from '../constants'
import { AppLocaleProviderProps } from '../types'
import React from 'react'

export default function AppLocaleProviderView({ locale, children }: AppLocaleProviderProps): JSX.Element {
    return <LocaleContext.Provider value={locale}>{children}</LocaleContext.Provider>
}
