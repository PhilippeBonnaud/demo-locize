import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { defaultLocale } from '../constants'
import { AppLocale } from '../types'
import { AppLocaleProviderProps } from '../types'

export type useAppLocaleProviderProps = {
    children: React.ReactNode
}

export const useAppLocaleProvider = ({ children }: useAppLocaleProviderProps): AppLocaleProviderProps => {
    const { locale: routerLocale } = useRouter()
    const [locale, setLocale] = useState<AppLocale>((routerLocale as AppLocale) || defaultLocale)
    
    console.log('Router', routerLocale)
    console.log('State', locale)

    useEffect(() => {
        console.log('Well changing locale', routerLocale);
        
        if (routerLocale !== locale) setLocale(routerLocale as AppLocale)
    }, [locale, routerLocale])

    return {
        children,
        locale,
    }
}
