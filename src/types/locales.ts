export type AppLocale = 'en' | 'de' | 'cs' | 'es'
export type AppLocaleDescription = Record<AppLocale, string> & {
    [key: string]: string
}
