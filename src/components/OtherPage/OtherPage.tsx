import { Box, Typography } from "@material-ui/core"
import { useTranslation, withTranslation } from "next-i18next"
import { AppProps } from "next/dist/shared/lib/router/router"
import SubComponent from "./components/SubComponents"

export const OtherPage = ( ) => {
    const { t } = useTranslation()
    return (
        <Box display='flex' flexDirection='column' alignContent={'center'} justifyContent="center">
            <Typography style={{ width:'100%', textAlign:'center'}}>{t('This is just another page.')}</Typography>
            <Box display='flex' flexDirection='column' alignContent={'center'} justifyContent="center">
                <SubComponent/>
            </Box>
        </Box>
    )
}
