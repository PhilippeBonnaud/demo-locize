import { useTranslation, withTranslation } from "next-i18next"
import { AppProps } from "next/dist/shared/lib/router/router"
import { Box } from '@material-ui/core';

const SubComponent = () => {
    const {t} = useTranslation()

    return <Box margin={'auto'}>{t('And sub component')}</Box>
}

export default SubComponent