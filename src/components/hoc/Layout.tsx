import { Button, Grid, Typography, Box } from '@material-ui/core'
import { useTranslation } from 'next-i18next'
import { useRouter } from 'next/router'
import useAppLocale from '../../hooks/useAppLocale'

type useLayout = {
    children: React.ReactNode
}

type LayoutProps = {
    children: React.ReactNode
}

type Lngs = { code: string; name: string }[]

const lngs: Lngs = [
    { code: 'en', name: 'English' },
    { code: 'de', name: 'Deutsch' },
    { code: 'es', name: 'Spanish' },
    { code: 'cs', name: 'Czech' },
]

const Layout: React.FC<LayoutProps> = ({ children }: useLayout) => {
    const { t, ready } = useTranslation()
    const router = useRouter()
    const locale = useAppLocale()
    console.log('locale', locale)
    if (!ready) return <>Loading Translations</>

    const isRoot = router.route === '/'

    return (
        <Box>
            <Box
                style={{
                    width: '100%',
                    display: 'flex',
                    flexDirection: 'row',
                    alignContent: 'center',
                    alignItems: 'center',
                    justifyContent: 'center',
                    height: '5rem',
                    backgroundColor: '#20427F',
                    background: 'transparent linear-gradient(149deg, #20427F 0%, #5477B5 100%) 0% 0% no-repeat padding-Grid',
                }}
            >
                <Grid
                    container
                    id='root'
                    style={{
                        alignItems: 'center',
                    }}
                >
                    <Grid id='header' item xs={9}>
                        <Typography
                            component='h2'
                            style={{
                                padding: '1.2rem',
                                fontSize: '1.5rem',
                                fontWeight: 'bold',
                                color: 'white',
                                letterSpacing: '.1rem',
                            }}
                        >
                            {t('Capitain.pocket')} : {locale}
                        </Typography>
                    </Grid>
                    <Grid item sm={3} style={{ display: 'flex', justifyContent: 'flex-end', padding: '0 0.5rem 0 0' }}>
                        {lngs.map((l) => {
                            return (
                                <Button
                                    key={l.code}
                                    onClick={() => router.push(router.pathname, router.asPath, { locale: l.code })}
                                    style={{ height: '25px', margin: '0.1rem', backgroundColor: '#5477B5', color: 'white' }}
                                >
                                    {t(l.name)}
                                </Button>
                            )
                        })}
                        {!isRoot && (
                            <Button
                                onClick={() => router.replace('/')}
                                style={{
                                    backgroundColor: '#5477B5',
                                    height: '25px',
                                    margin: '0 0 0 0.5rem',
                                }}
                            >
                                {t('Back')}
                            </Button>
                        )}
                    </Grid>
                </Grid>
            </Box>
            <Box id='content' style={{ padding: '5rem' }}>
                {children}
            </Box>
        </Box>
    )
}

export default Layout
