import { useContext } from 'react'
import { AppLocale } from '../types/locales'
import { LocaleContext } from '../providers/AppLocaleProvider/constants'

const useAppLocale = (): AppLocale => {
    return useContext(LocaleContext)
}
export default useAppLocale
