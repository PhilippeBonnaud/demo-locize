import { t, Translation } from '../translations'
import useAppLocale from './useAppLocale'

const useTranslations = (): Translation => {
    const locale = useAppLocale()
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    return (t as any)[locale]
}
export default useTranslations
