const LocizeBackend = require('i18next-locize-backend/cjs')

module.exports = {
    debug: true,
    i18n: {
        // These are all the locales you want to support in
        // your application
        locales: ['de', 'en', 'cs', 'es'],
        // This is the default locale you want to be used when visiting
        // a non-locale prefixed path e.g. `/hello`
        defaultLocale: 'en',
        // this will download the translations from locize directly, in client (browser) and server (node.js)
        // DO NOT USE THIS if having a serverless environment => this will generate too much download requests
        //   => https://github.com/locize/i18next-locize-backend#important-advice-for-serverless-environments---aws-lambda-google-cloud-functions-azure-functions-etc
    },
    backend: {
        loadPath: 'https://api.locize.io/{{projectId}}/{{version}}/{{lng}}/common',
        addPath: 'https://api.locize.io/missing/{{projectId}}/{{version}}/{{lng}}/common',
        allowedAddOrUpdateHosts: ['localhost'],
        onSaved: (lng, ns) => {
            console.log(`Saved ${lng}, ${ns}`);
        },
        projectId: 'a98184c5-e9ed-47c0-8ae7-69a5af653f1d',
        apiKey: '5a838d4b-b44a-4842-8f97-684a274d1a81', // to not add the api-key in production, used for saveMissing feature
        referenceLng: 'en',
    },
    use: [LocizeBackend, require('locize').locizePlugin],
    ns: ['common'], // the namespaces needs to be listed here, to make sure they got preloaded
    defaultNs: 'common',
    serializeConfig: false, // because of the custom use i18next plugin
    saveMissing: true, // do not saveMissing to true for production
    debug: true,
}
