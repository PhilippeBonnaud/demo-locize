import { Box, CircularProgress } from '@material-ui/core'
import { useTranslation } from 'next-i18next'
import Head from 'next/head'

export default function NewPage(): JSX.Element {
    const { t, ready } = useTranslation('common')
    if (!ready) <CircularProgress />

    return (
        <>
            <Head>
                <title>{t('PageTitle-NewPage')}</title>
            </Head>
            <Box display='flex' flexDirection={'column'}>
                <Box padding='5px' margin='auto'>
                    {t('New page here')}
                </Box>
                <Box padding='5px' margin='auto' style={{ width: '25rem' }}>
                    {t('newPage.longExplanationAboutWhatAPageIs')}
                </Box>
                <Box padding='5px' margin='auto'>
                    Some text not wrapped in t function
                </Box>
            </Box>
        </>
    )
}
