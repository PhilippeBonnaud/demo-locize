import { Box, CircularProgress } from '@material-ui/core'
import { useTranslation } from 'next-i18next'
import Head from 'next/head'
import useTranslations from '../../src/hooks/useTranslations'

export default function NewPage(): JSX.Element {
    const t = useTranslations()

    return (
        <>
            <Head>
                <title>{t['PageTitle-OldPage']}</title>
            </Head>
            <Box display='flex' flexDirection={'column'}>
                <Box padding='5px' style={{ width: '25rem' }}>
                    {t['This page is still translated using the current custom hook']}
                </Box>
                <Box padding='5px'>
                    {t['Meaning there are no conflicts and we can refactor incrementally once the software is integrated']}
                </Box>
            </Box>
        </>
    )
}
