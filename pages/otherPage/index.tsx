import React from 'react'
import Head from 'next/head'
import { AppProps } from 'next/dist/shared/lib/router/router'
// import { serverSideTranslations } from 'next-i18next/serverSideTranslations'
import { OtherPage as OtherPageComponent } from '../../src/components/OtherPage/OtherPage'
import { useTranslation } from 'next-i18next'
import { Box, CircularProgress } from '@material-ui/core'

export default function Connectivity(): JSX.Element {
    const { t, ready } = useTranslation()
    if (!ready) <CircularProgress />

    return (
        <>
            <Head>
                <title>{t('PageTitle-OtherPage')}</title>
            </Head>
            <Box padding='5px' className={'container'}>
                <OtherPageComponent />
            </Box>
        </>
    )
}

// export const getStaticProps = async ({ locale }: AppProps) => ({
//     props: {
//         ...(await serverSideTranslations(locale)),
//     },
// )
