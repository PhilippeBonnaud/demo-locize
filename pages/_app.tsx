import '../styles/globals.css'
import type { AppProps } from 'next/app'
import { appWithTranslation } from 'next-i18next'
import nextI18nextConfig from '../next-i18next.config'
import '../next-i18next.config'
import { Suspense } from 'react'
import Layout from '../src/components/hoc/Layout'
import AppLocaleProvider from '../src/providers/AppLocaleProvider/AppLocaleProvider'

function MyApp({ Component, pageProps }: AppProps) {
    return (
        <AppLocaleProvider>
            <Layout>
                <Component {...pageProps} />
            </Layout>
        </AppLocaleProvider>
    )
}

export default appWithTranslation(MyApp, nextI18nextConfig)
